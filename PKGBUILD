# Maintainer: Antoni Aloy <aaloytorrens@gmail.com>
# See https://github.com/dannye/crystal-tracker/blob/788ccad/INSTALL.md
# Seems that crystal-tracker needs very specific software versions
# Upstream recommends to install from latest master
pkgname=crystal-tracker
pkgver=0.7.0+git20240624.381d541
pkgrel=4
_commit="381d541bc1f487862ef338e3a1ce932b34aca3cf"
pkgdesc='Song and sound editor for pokecrystal-based sound engines'
arch=("amd64")
license=("LGPL-3.0")
url="https://github.com/dannye/crystal-tracker"
makedepends=("autoconf" "cmake" "libpng-dev" "libxpm-dev" "zlib1g-dev" "libx11-dev" "libxft-dev" "libxinerama-dev" "libfontconfig1-dev" "x11proto-xext-dev" "libxrender-dev" "libxfixes-dev" "libgl1-mesa-dev" "libglu1-mesa-dev" "libpulse-dev" "libasound2-dev")
depends=("zlib1g" "libxpm4" "libpng16-16" "libx11-6" "libxft2" "libxinerama1" "libfontconfig1" "libxrender1" "libxfixes3" "libasound2" "libasound2-plugins" "libpulse0")
# Pulseaudio does not work well with the program
conflicts=("pulseaudio")
# Inner dependencies in $pkgname_$pkgver in order to move them easily later
source=(
	"0001-crystal-tracker-usr-prefix.patch"
	"0002-correct-desktop-file.patch"
	"$pkgname-$pkgver.tar.gz::https://github.com/dannye/crystal-tracker/archive/381d541.tar.gz"
	"fltk_1.4_git20240623.tar.gz::https://github.com/fltk/fltk/archive/1c482b5dbf38aa38ec323dad0869343a58237245.tar.gz"
	"portaudio_19.7.0.tar.gz::https://github.com/PortAudio/portaudio/archive/refs/tags/v19.7.0.tar.gz"
	"libopenmt_0.6.3.tar.gz::https://lib.openmpt.org/files/libopenmpt/src/libopenmpt-0.6.3+release.autotools.tar.gz"
)
sha256sums=('75b7b85dfed7592dbe3eaea4ff21f81d5cb09a68af0ae685aa8414ded7fa50a4'
            '77481d6ec9c3ac4b084b586942c039a69a0b87f6c9a98c76e4d869d38394d7ce'
            'dc9872753248d89e64cd84be2aa9d4f29ee33d2bdf64c3d73ced44ab2bfb3e1b'
            '7d2473379646d48281ca9aba2fb4606bf83e1434d69ecd34ab190bae3b929274'
            '5af29ba58bbdbb7bbcefaaecc77ec8fc413f0db6f4c4e286c40c3e1b83174fa0'
            'a410afeb7cd5970b16b9a6ce7daccf495b1a5e9121a9d6442de0c028fd54a25b')

prepare() {
	cd $srcdir
	# Move dependencies
	[ -d $pkgname-$_commit/lib/fltk ] || mv fltk-* $pkgname-$_commit/lib/fltk
	[ -d $pkgname-$_commit/lib/portaudio ] || mv portaudio-* $pkgname-$_commit/lib/portaudio
	[ -d $pkgname-$_commit/lib/libopenmpt ] || mv libopenmpt-* $pkgname-$_commit/lib/libopenmpt

	# Apply patches
	cd crystal-tracker-$_commit/lib/fltk
	yes | patch -p1 < ../patches/fltk.patch

	cd $srcdir/crystal-tracker-$_commit
	yes | patch -p1 < $srcdir/0001-crystal-tracker-usr-prefix.patch
	yes | patch -p1 < $srcdir/0002-correct-desktop-file.patch
}

build() {
	cd $pkgname-$_commit/lib/fltk
	# Build dependencies
	# FLTK
	cmake -D CMAKE_INSTALL_PREFIX="$(realpath "$PWD/../..")" -D CMAKE_BUILD_TYPE=Release -D FLTK_BUILD_TEST=OFF
	make
	make install

	# PORTAUDIO
	cd ../portaudio
	./configure --prefix="$(realpath "$PWD/../..")" CXXFLAGS="-O2" CFLAGS="-O2"
	make
	make install
	cd bindings/cpp
	./configure --prefix="$(realpath "$PWD/../../../..")" CXXFLAGS="-O2" CFLAGS="-O2"
	make
	make install
	cd ../..

	# Move to main package
	cp include/pa_linux_alsa.h include/portaudio.h ../../include/portaudiocpp

	# OPENMPT
	cd ../libopenmpt
	./configure --prefix="$(realpath "$PWD/../..")" \
		--without-mpg123 \
		--without-ogg \
		--without-vorbis \
		--without-vorbisfile \
		--without-sndfile \
		--without-flac \
		CXXFLAGS="-O2" CFLAGS="-O2" PKG_CONFIG_PATH="$(realpath "$PWD/../../lib/pkgconfig")"
	make
	make install

	# End build dependencies

	cd ../..

	# Build main package
	make PREFIX=/usr
}

package() {
	cd $pkgname-$_commit
	make DESTDIR="$pkgdir" install
}


# vim: set ts=2 sw=2 et:
